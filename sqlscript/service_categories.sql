-- ----------------------------
-- Table structure for `service_categories`
-- ----------------------------
DROP TABLE IF EXISTS `service_categories`;
CREATE TABLE `service_categories` (
  `serviceCategoryId` int(10) NOT NULL AUTO_INCREMENT,
  `serviceCategoryName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`serviceCategoryId`)
);

-- ----------------------------
-- Table structure for `services`
-- ----------------------------
DROP TABLE IF EXISTS `services`;
CREATE TABLE `services` (
  `serviceId` int(10) NOT NULL AUTO_INCREMENT,
  `serviceCategoryId` int(10) DEFAULT NULL,
  `serviceName` varchar(255) DEFAULT NULL,
  `serviceSupplierReference` varchar(255) DEFAULT NULL,
  `servicePricingLevel` varchar(255) DEFAULT NULL,
  `serviceDuration` varchar(255) DEFAULT NULL,
  `serviceFullPrice` varchar(255) DEFAULT NULL,
  `serviceSalePrice` varchar(255) DEFAULT NULL,
  `servicePrimaryImge` varchar(255) DEFAULT NULL,
  `serviceDescription` text,
  `serviceRestrictions` text,
  `serviceGoodToKnow` text,
  PRIMARY KEY (`serviceId`)
);





//11 Dec 2014 8:27 Added New Columns
ALTER TABLE services ADD type VARCHAR(15);
ALTER TABLE services ADD serviceIds VARCHAR(5000);



//13 Dec 2014 4:30 PM

CREATE TABLE `user_services` (
  `userId` int(10) DEFAULT NULL,
  `serviceCategoryId` int(10) DEFAULT NULL,
  `serviceId` int(10) DEFAULT NULL
);


CREATE TABLE `users` (
  `userId` int(10) NOT NULL AUTO_INCREMENT,
  `userName` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `fullName` varchar(255) DEFAULT NULL,
  `userRole` varchar(255) DEFAULT NULL,
  `jobTitle` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `about` text,
  `serviceIds` varchar(4000) DEFAULT NULL,
  PRIMARY KEY (`userId`)
);