<?php
namespace com\kaic\controllers;
class UserController{
	public static $tableName="users";
	public static $userServicesTable="user_services";
	public $userId;
	public $userName;
	public $email;
	public $fullName;
	public $userRole;
	public $jobTitle;
	public $phone;
	public $gender;
	public $about;
	
	
	
	
	public function createTeamMember(){
		$tableName=self::$tableName;
		$userName=$_POST['email'];
		$email=$_POST['email'];
		$fullName=$_POST['fullName'];
		$userRole="team_member";
		$jobTitle=$_POST['jobTitle'];
		$phone=$_POST['phone'];
		$gender=$_POST['gender'];
		$about=$_POST['about'];
		$selectedServiceIds=$_POST['services'];
		
		$result=false;
		
		$sql="INSERT INTO {$tableName} (
		userName,
		email,
		fullName,
		userRole,
		jobTitle,
		phone,
		gender,
		about
		) values (
		'{$userName}',
		'{$email}',
		'{$fullName}',
		'{$userRole}',
		'{$jobTitle}',
		'{$phone}',
		'{$gender}',
		'{$about}'
		)";
		$mysqlObject=new \com\kaic\connection\MySQLConnection();
		$userId=$mysqlObject->executeQueryNGetInsertID($sql);
		
		if($userId>0){
			$result=true;
			if(count($selectedServiceIds)>0){
				$tableName=self::$userServicesTable;
				foreach($selectedServiceIds as $serviceId){
					$sql="INSERT INTO {$tableName} (
					userId,
					serviceId
					) values (
					'{$userId}',
					'{$serviceId}'
					)";
					$mysqlObject=new \com\kaic\connection\MySQLConnection();
					$id=$mysqlObject->executeQueryNGetInsertID($sql);
				}
			}
			
		}
		return $result;
	}
	
	
	
	public function getTeamMembersList(){
		$tableName=self::$tableName;
		$sql="SELECT * FROM {$tableName} WHERE userRole='team_member'";
		$mysqlObject=new \com\kaic\connection\MySQLConnection();
		return $mysqlObject->executeQuery($sql);
	}
	
}