<?php
namespace com\kaic\controllers;

class ServicesController{
	public static $tableName="services";
	
	public $serviceName;
	public $supplierReference;
	public $pricingLevel;
	public $duration;
	public $fullPrice;
	public $salePrice;
	public $primaryImge;
	public $description;
	public $restrictions;
	public $goodToKnow;
	
	
	public function getImageExtension($imagetype)
   	 {
       if(empty($imagetype)) return false;
       switch($imagetype)
       {
           case 'image/bmp': return '.bmp';
           case 'image/gif': return '.gif';
           case 'image/jpeg': return '.jpg';
           case 'image/png': return '.png';
           default: return false;
       }
     }
	
	public function create(){
		$target_path="";
		if (!empty($_FILES["servicePrimaryImge"]["name"])) {
			$file_name=$_FILES["servicePrimaryImge"]["name"];
			$temp_name=$_FILES["servicePrimaryImge"]["tmp_name"];
			$imgtype=$_FILES["servicePrimaryImge"]["type"];
			$ext= $this->getImageExtension($imgtype);
			$imagename=date("d-m-Y")."-".time().$ext;
			$target_path = "uploads/".$imagename;
			if(move_uploaded_file($temp_name, $target_path)) {
			
			}
		}
		
		$tableName=self::$tableName;
		$serviceName=$_POST['serviceName'];
		$serviceCategoryId=$_GET['serviceCategoryId'];
		$serviceSupplierReference=$_POST['serviceSupplierReference'];
		$servicePricingLevel=$_POST['servicePricingLevel'];
		$serviceDuration=$_POST['serviceDuration'];
		$serviceFullPrice=$_POST['serviceFullPrice'];
		$serviceSalePrice=$_POST['serviceSalePrice'];
		$servicePrimaryImge=$target_path;
		$serviceDescription=$_POST['serviceDescription'];
		$serviceRestrictions=$_POST['serviceRestrictions'];
		$serviceGoodToKnow=$_POST['serviceGoodToKnow'];
		
		
		
		$sql="INSERT INTO {$tableName} (
		serviceName,
		serviceCategoryId,
		serviceSupplierReference,
		servicePricingLevel,
		serviceDuration,
		serviceFullPrice,
		serviceSalePrice,
		servicePrimaryImge,
		serviceDescription,
		serviceRestrictions,
		serviceGoodToKnow,
		type
		
		) values (
		'{$serviceName}',
		{$serviceCategoryId},
		'{$serviceSupplierReference}',
		'{$servicePricingLevel}',
		'{$serviceDuration}',
		'{$serviceFullPrice}',
		'{$serviceSalePrice}',
		'{$servicePrimaryImge}',
		'{$serviceDescription}',
		'{$serviceRestrictions}',
		'{$serviceGoodToKnow}',
		'service'
		)";
		$mysqlObject=new \com\kaic\connection\MySQLConnection();
		return $mysqlObject->execute($sql);
	}
	
	
	public function getServiceListByCategoryID($serviceCategoryId,$type=""){
		
		$conditionsList=array();
		$conditionsList[]="serviceCategoryId={$serviceCategoryId}";
		
		if(!empty($type)){
			$conditionsList[]="type='{$type}'";
		}
		
		$conditionQuery=implode(' AND ', $conditionsList);
		
		$tableName=self::$tableName;
		$sql="SELECT * FROM {$tableName} WHERE $conditionQuery";
		//echo $sql;
		$mysqlObject=new \com\kaic\connection\MySQLConnection();
		return $mysqlObject->executeQuery($sql);
	}
	
	
	
	
	
	
	//Store the Selected Treatment Services in the Session ['Service Category ID']
	public function saveTreatments(){
		$serviceCategoryId=$_GET['serviceCategoryId'];
		$selectedServiceIds=array();
		foreach($_POST as $key => $value){
			
			$regEx = "/^(treatment_)?\d+$/";
			if (preg_match($regEx, $key)) {
				if($value=="on"){
					$tArray = explode('treatment_', $key);
					$serviceId = $tArray[1];
					$selectedServiceIds[]=$serviceId;
				}
			}
		}
		
		
		$_SESSION['selectedServiceIds'.$serviceCategoryId]=$selectedServiceIds;
		
		
	}
	
	public function getSavedTreatments($serviceCategoryId){
		if(isset($_SESSION['selectedServiceIds'.$serviceCategoryId])){
			if(count($_SESSION['selectedServiceIds'.$serviceCategoryId])){
				$serviceIds=implode(',', $_SESSION['selectedServiceIds'.$serviceCategoryId]);
			}
		}
		return $serviceIds;
	}
	
	
	
	public function createPackage(){
		$tableName=self::$tableName;
		$serviceName=$_POST['serviceName'];
		$serviceCategoryId=$_GET['serviceCategoryId'];
		$serviceSupplierReference=$_POST['serviceSupplierReference'];
		$servicePricingLevel=$_POST['servicePricingLevel'];
		$serviceDuration=$_POST['serviceDuration'];
		$serviceFullPrice=$_POST['serviceFullPrice'];
		$serviceSalePrice=$_POST['serviceSalePrice'];
		$serviceDescription=$_POST['serviceDescription'];
		$serviceRestrictions=$_POST['serviceRestrictions'];
		$serviceGoodToKnow=$_POST['serviceGoodToKnow'];
		$serviceIds=$this->getSavedTreatments($serviceCategoryId);
	
		$sql="INSERT INTO {$tableName} (
		serviceName,
		serviceCategoryId,
		serviceSupplierReference,
		servicePricingLevel,
		serviceDuration,
		serviceFullPrice,
		serviceSalePrice,
		serviceDescription,
		serviceRestrictions,
		serviceGoodToKnow,
		type,
		serviceIds
	
		) values (
		'{$serviceName}',
		{$serviceCategoryId},
		'{$serviceSupplierReference}',
		'{$servicePricingLevel}',
		'{$serviceDuration}',
		'{$serviceFullPrice}',
		'{$serviceSalePrice}',
		'{$serviceDescription}',
		'{$serviceRestrictions}',
		'{$serviceGoodToKnow}',
		'package',
		'{$serviceIds}'
		)";
		$mysqlObject=new \com\kaic\connection\MySQLConnection();
		return $mysqlObject->execute($sql);
	}
	
	
	
	
	
}