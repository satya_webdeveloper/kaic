<?php
namespace com\kaic\connection;
class MySQLConnection{
	const USERNAME="root";
	const PASSWORD="";
	const DBNAME="kaic";
	const HOST="localhost";
	

	
	public static function getConnection(){
		$connection = mysqli_connect(self::HOST, self::USERNAME, self::PASSWORD, self::DBNAME);
		return $connection;
	}
	
	public function execute($sql){
		$conn=MySQLConnection::getConnection();
		return mysqli_query($conn, $sql);
	}
	
	
	public function executeQuery($sql){
		$conn=MySQLConnection::getConnection();
		$result= mysqli_query($conn, $sql);
		
		$data=array();
		if (mysqli_num_rows($result) > 0) {
			// output data of each row
			while($row = mysqli_fetch_assoc($result)) {
				$data[]=$row;
			}
		}
		
		return $data;
	}
	
	
	public function executeQueryNGetInsertID($sql){
		$conn=MySQLConnection::getConnection();
		mysqli_query($conn, $sql);
		return mysqli_insert_id($conn);
	}
	
	

	
}

