<?php
include_once '../router.php';
include_once('header.inc');

?>





            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Menu
                    </h1>
					<ol class="breadcrumb">
                        <li><button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#add-group-modal"><i class="fa fa-plus"></i>&nbsp;&nbsp;&nbsp;Add a group</button></li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
					
					<?php 
					$serviceCategories=new com\kaic\controllers\ServiceCategoriesController();
					$servicesObject=new com\kaic\controllers\ServicesController();
						
					//
					
					
					$serviceCategoriesList=$serviceCategories->getServiceCategoriesList();
					foreach($serviceCategoriesList as $serviceCategory){
					?>
					<!-- Face box -->
					<div class="box box-solid box-primary">
						<div class="box-header">
							<h3 class="box-title"><?=$serviceCategory['serviceCategoryName'];?></h3>
							<div class="box-tools pull-right">
								<button class="btn btn-primary btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
								<button class="btn btn-primary btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
							</div>
						</div>
						<div class="box-body">
							<div class="row">
							  <div class="col-lg-12 col-md-12 text-right">
									<button class="btn btn-success btn-sm" onclick="addNewService('<?=$serviceCategory['serviceCategoryName']?>','<?=$serviceCategory['serviceCategoryId']?>')"><i class="fa fa-plus"></i>&nbsp;&nbsp;&nbsp;Add new service</button>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-12 col-md-12">
									<table class="table table-condensed">
							<?php 
							$serviceList=$servicesObject->getServiceListByCategoryID($serviceCategory['serviceCategoryId']);
							foreach($serviceList as $service){
							?>
								<tr>
									<td width="70%"><?=$service['serviceName']?></td>
									<td width="10%"><?=$service['serviceDuration']?></td>
									<td width="10%">�<?=$service['serviceFullPrice']?></td>
									<td width="10%">
									<button class="btn btn-info btn-sm" data-toggle="modal" data-target="#add-new-service-modal">Edit</button>
									</td>
									<td width="10%">
									<button class="btn btn-danger btn-sm">Delete</button>
									</td>
									</tr>
							<?php	
							}
							?>
							</table>
								</div>
							</div>
							
							
							
						</div><!-- /.box-body -->
					</div><!-- /.box -->
						<?php 
					}
					?>
					<!-- hidden Variables -->
					<input type="hidden" id="serviceCategoryNameHidden" value="">
					<input type="hidden" id="serviceCategoryIdHidden" value="">
								
					
					
					
					
					
					
					
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->
		
		<!-- Create Menu Group MODAL -->
		<form id="serviceCategoryForm" action="../router.php" method="post">
        <div class="modal fade" id="add-group-modal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Add Menu Group</h4>
                    </div>
                    <form action="#" method="post">
                        <div class="modal-body">
                                    <div class="box-body">
                                        <div class="form-group">
											<div  class="col-md-3 pull-left label-adjust"><label for="">Group Name</label></div>
                                            <div  class="col-md-9"><input id="serviceCategoryName" type="text" name="serviceCategoryName" class="form-control"></div>
                                        </div>
                                    </div><!-- /.box-body -->

                        </div>
                        <div class="modal-footer clearfix">

                            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Cancel</button>

                            <button id="serviceCategoryAddBtn" type="submit" class="btn btn-primary pull-left"><i class="fa fa-check"></i> Submit</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
		</form>
		
		<!-- Create New Service MODAL -->
        <div class="modal fade" id="new-service-modal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">What would you like to create?</h4>
                    </div>
                    <form action="#" method="post">
                        <div class="modal-body">
                                    <div class="box-body">
										<div class="form-group">
											<div  class="col-md-3 pull-left label-adjust">
												<button class="btn btn-info" data-toggle="modal" data-target="#add-new-service-modal">Service</button>
											</div>
                                            <div  class="col-md-9">A service is just one treatment or activity per appointment - nice and simple.</div>
                                        </div>
										<div class="form-group">
											<div  class="col-md-3 pull-left label-adjust">
												<button class="btn btn-info" data-toggle="modal" data-target="#add-new-package-modal">Package</button>
											</div>
                                            <div  class="col-md-9">A package is a mix of experiences or treatments, such as a cut and blow dry.</div>
                                        </div>
                                    </div><!-- /.box-body -->
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
		
		
		
		<!-- Add New Service MODAL -->
		<form id="serviceForm" action="../router.php" method="post">
        <div class="modal fade" id="add-new-service-modal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Service</h4>
                    </div>
                    <form action="#" method="post">
                        <div class="modal-body">
                                    <div class="box-body scroll-visible">
										<div class="form-group">
											<div  class="col-md-12 pull-left">
												<input type="text" name="serviceName" id="serviceName" class="form-control" placeholder="Service Name" />
											</div>
                                        </div>
										<div class="clear"></div>
										<div class="form-group">
											<div  class="col-md-12 pull-left">
												<input type="text" name="serviceSupplierReference" id="serviceSupplierReference" class="form-control" placeholder="Supplier reference" />
											</div>
                                        </div>
										<div class="clear"></div>
										<hr/>
										<div class="form-group">
											<div  class="col-md-12 pull-left">
												<h4>Price</h4>
											</div>
                                        </div>
										<div class="clear"></div>
										<div class="form-group">
											<div  class="col-md-3 pull-left label-adjust"><label for="">Pricing Level</label></div>
                                            <div  class="col-md-9">
												<select class="form-control" id="servicePricingLevel"  name="servicePricingLevel">
													<option value="Same price for all staff">Same price for all staff</option>
												</select>
											</div>
                                        </div>
										<div class="clear"></div>
										<div class="form-group">
											<div  class="col-md-3 pull-left label-adjust"><label for="">Duration</label></div>
                                            <div  class="col-md-9">
												<select class="form-control" id="serviceDuration" name="serviceDuration">
													<option>1 h </option>
													<option>1 h 10 min</option>
													<option>1 h 20 min</option>
													<option>1 h 30 min</option>
													<option>1 h 40 min</option>
													<option>1 h 50 min</option>
												</select>
											</div>
                                        </div>
										<div class="clear"></div>
										<div class="form-group">
											<div class="col-md-3 pull-left label-adjust"><label for="">Full price, �</label></div>
                                            <div class="col-md-3">
												<input type="text" name="serviceFullPrice" id="serviceFullPrice" class="form-control" />
											</div>
											<div class="col-md-3 label-adjust">
												<label for="">Sale price, �</label>
											</div>
											<div class="col-md-3">
												<input type="text" name="serviceSalePrice" id="serviceSalePrice" class="form-control" />
											</div>
                                        </div>
										<div class="clear"></div>
										<div class="form-group">
											<div  class="col-md-3 pull-left label-adjust"><label for="">Primary Image</label></div>
                                            <div  class="col-md-9">
												<input type="file" name="servicePrimaryImge" id="servicePrimaryImge" />
											</div>
                                        </div>
										<div class="clear"></div>
										<hr/>
										<div class="form-group">
											<div  class="col-md-3 pull-left label-adjust"><label for="">Description</label></div>
                                            <div  class="col-md-9">
												<textarea class="form-control" name="serviceDescription" id="serviceDescription" rows="3" placeholder=""></textarea>
											</div>
                                        </div>
										<div class="clear"></div>
										<div class="form-group">
											<div  class="col-md-3 pull-left label-adjust"><label for="">Restrictions</label></div>
                                            <div  class="col-md-9">
												<textarea class="form-control" name="serviceRestrictions"  id="serviceRestrictions" rows="3" placeholder=""></textarea>
											</div>
                                        </div>
										<div class="clear"></div>
										<div class="form-group">
											<div  class="col-md-3 pull-left label-adjust"><label for="">Good to Know</label></div>
                                            <div  class="col-md-9">
												<textarea class="form-control" name="serviceGoodToKnow" id="serviceGoodToKnow" rows="3" placeholder=""></textarea>
											</div>
                                        </div>
                                    </div><!-- /.box-body -->
                        </div>
						<div class="modal-footer clearfix">

                            <button type="button"  class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Cancel</button>

                            <button type="submit"  id="serviceAddBtn" class="btn btn-primary pull-left"><i class="fa fa-check"></i> Submit</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
		</form>
		
		<!-- Add New Package Modal Popup -->
		<form id="packageForm" action="../router.php" method="post">
		<div class="modal fade" id="add-new-package-modal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" style="width: 800px;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Package</h4>
                    </div>
                    <form action="#" method="post">
                        <div class="modal-body">
                                    <div class="box-body scroll-visible">
										<div class="form-group">
											<div  class="col-md-12 pull-left">
												<input type="text" name="serviceName" class="form-control" placeholder="Package Name" />
											</div>
                                        </div>
										<div class="clear"></div>
										<div class="form-group">
											<div class="col-md-12 pull-left">
												<input type="text" name="serviceSupplierReference" class="form-control" placeholder="Supplier reference" />
											</div>
                                        </div>
										<div class="clear"></div>
										<hr/>
										<div class="form-group">
											<div class="col-md-12 pull-left">
											<h4>Package includes</h4>
											<p>Assign up to ten treatments to the offer. This will allow customers to find it easier.</p>
											<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#select-services-modal">
											<i class="fa fa-plus"></i> Add Treatments</button>
											</div>
                                        </div>
										<div class="clear"></div>
										<hr/>
										<div class="form-group">
											<div class="col-md-12 pull-left">
											<h4>Price</h4>
											</div>
                                        </div>
										<div class="clear"></div>
										<div class="form-group">
											<div  class="col-md-3 pull-left label-adjust"><label for="">Pricing Level</label></div>
                                            <div  class="col-md-9">
												<select class="form-control" name="servicePricingLevel">
													<option value="Same price for all staff">Same price for all staff</option>
												</select>
											</div>
                                        </div>
										<div class="clear"></div>
										<div class="form-group">
											<div  class="col-md-3 pull-left label-adjust"><label for="">Duration</label></div>
                                            <div  class="col-md-9">
												<select class="form-control" name="serviceDuration">
													<option>1 h </option>
													<option>1 h 10 min</option>
													<option>1 h 20 min</option>
													<option>1 h 30 min</option>
													<option>1 h 40 min</option>
													<option>1 h 50 min</option>
												</select>
											</div>
                                        </div>
										<div class="clear"></div>
										<div class="form-group">
											<div class="col-md-3 pull-left label-adjust"><label for="">Full price, �</label></div>
                                            <div class="col-md-3">
												<input type="text" name="serviceFullPrice" class="form-control" />
											</div>
											<div class="col-md-3 label-adjust">
												<label for="">Sale price, �</label>
											</div>
											<div class="col-md-3">
												<input type="text" name="serviceSalePrice" class="form-control" />
											</div>
                                        </div>
										<div class="clear"></div>
										<hr/>
										<div class="form-group">
											<div  class="col-md-3 pull-left label-adjust"><label for="">Description</label></div>
                                            <div  class="col-md-9">
												<textarea class="form-control" name="serviceDescription" rows="3" placeholder=""></textarea>
											</div>
                                        </div>
										<div class="clear"></div>
										<div class="form-group">
											<div  class="col-md-3 pull-left label-adjust"><label for="">Restrictions</label></div>
                                            <div  class="col-md-9">
												<textarea class="form-control" name="serviceRestrictions" rows="3" placeholder=""></textarea>
											</div>
                                        </div>
										<div class="clear"></div>
										<div class="form-group">
											<div  class="col-md-3 pull-left label-adjust"><label for="">Good to Know</label></div>
                                            <div  class="col-md-9">
												<textarea class="form-control" name="serviceGoodToKnow" rows="3" placeholder=""></textarea>
											</div>
                                        </div>
                                    </div><!-- /.box-body -->
                        </div>
						<div class="modal-footer clearfix">

                            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Cancel</button>

                            <button type="submit" class="btn btn-primary pull-left"><i class="fa fa-check"></i> Submit</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
		</form>
		
			
		<!-- Select Services fo Package Modal Popup -->
		<form id="packageTreatmentsForm" action="../router.php" method="post">
		<div class="modal fade" id="select-services-modal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Select Services Included in the Package</h4>
                    </div>
                    
                        <div class="modal-body">
							<div class="box-body scroll-visible">
								<div class="col-md-12">
									<span class="cta-arrow"><i class="fa fa-exclamation"></i></span><span>Up to ten treatments can be selected.</span>
									<br/><br/>
									<!-- Custom Tabs -->
									<div class="nav-tabs-custom">
										
										
										<ul class="nav nav-tabs">
											<?php
											//Showing Service Categories Tabs 
											$serviceCategoriesList=$serviceCategories->getServiceCategoriesList();
											$i=1;
											foreach($serviceCategoriesList as $serviceCategory){
												if($i==1){
													echo '<li class="active"><a href="#treatments_'.$serviceCategory['serviceCategoryId'].'" data-toggle="tab">'.$serviceCategory['serviceCategoryName'].'</a></li>';
												}else{
													echo '<li><a href="#treatments_'.$serviceCategory['serviceCategoryId'].'" data-toggle="tab">'.$serviceCategory['serviceCategoryName'].'</a></li>';
												}
												$i++;
											}
											?>
										</ul>
										
										
										<div class="tab-content">
											<?php 
											$serviceCategoriesList=$serviceCategories->getServiceCategoriesList();
											$showDefaultTab="active";
											foreach($serviceCategoriesList as $serviceCategory){
												echo '<div class="tab-pane '.$showDefaultTab.'" id="treatments_'.$serviceCategory['serviceCategoryId'].'">';
												$showDefaultTab='';
												$serviceList=$servicesObject->getServiceListByCategoryID($serviceCategory['serviceCategoryId'],'service');
												foreach($serviceList as $service){
													echo '<div class="checkbox">
															<label>
																<input name="treatment_'.$service['serviceId'].'" type="checkbox"/>
																&nbsp;'.$service["serviceName"].'
															</label>                
														  </div>';
												}
												echo '</div><!-- /.tab-pane -->';
											}
											?>
										</div><!-- /.tab-content -->
									</div><!-- nav-tabs-custom -->
								</div><!-- /.col -->
								<div class="clear"></div>
							</div><!-- /.box-body -->
                        </div>
						<div class="modal-footer clearfix">
						<button type="submit" class="btn btn-primary pull-left"><i class="fa fa-check"></i> Submit</button>
                      	<button type="button" class="btn btn-danger pull-right" data-dismiss="modal"><i class="fa fa-times"></i>Cancel</button>
                        </div>
                   
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        </form>
		
		
        
        
<?php
include_once('footer.inc');
?>


<script>






$("#serviceCategoryForm").on('submit',(function(e){
	e.preventDefault();
	$.ajax({
	url: '../router.php?controller=ServiceCategoriesController&cmd=create',
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(response, status, req) {
		if(response==1){
			alert("Group Name Added Sucessfully");
		}
	  },
	error: function(){} 	        
	});
}));






function addNewService(serviceCategoryName,serviceCategoryId){
	$('#serviceCategoryNameHidden').val(serviceCategoryName);
	$('#serviceCategoryIdHidden').val(serviceCategoryId);
	$('#new-service-modal').modal('show');
}


	

$("#serviceForm").on('submit',(function(e){
	var serviceCategoryId=$('#serviceCategoryIdHidden').val().trim();
	e.preventDefault();
	$.ajax({
	url: '../router.php?controller=ServicesController&cmd=create&serviceCategoryId='+serviceCategoryId,
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(response, status, req) {
		if(response==1){
			alert("Service Name Added Sucessfully");
		}
	  },
	error: function(){} 	        
	});
}));


$("#packageForm").on('submit',(function(e){
	var serviceCategoryId=$('#serviceCategoryIdHidden').val().trim();
	e.preventDefault();
	$.ajax({
	url: '../router.php?controller=ServicesController&cmd=createPackage&serviceCategoryId='+serviceCategoryId,
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(response, status, req) {
		if(response==1){
			alert("Service Name Added Sucessfully");
		}
	  },
	error: function(){} 	        
	});
}));




$("#packageTreatmentsForm").on('submit',(function(e){
	var serviceCategoryId=$('#serviceCategoryIdHidden').val().trim();
	e.preventDefault();
	$.ajax({
	url: '../router.php?controller=ServicesController&cmd=saveTreatments&serviceCategoryId='+serviceCategoryId,
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(response, status, req) {
		if(response==1){
			alert("Service Name Added Sucessfully");
		}
	  },
	error: function(){} 	        
	});
}));



    
</script>
