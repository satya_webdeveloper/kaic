<?php
include_once '../router.php';
include_once('header.inc');

?>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Team
                    </h1>
					<ol class="breadcrumb">
                        <li><button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#add-team-member-modal"><i class="fa fa-plus"></i>&nbsp;&nbsp;&nbsp;Add team member</button></li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
					
					<?php 
						$imgSrc=array('male'=>'../img/avatar5.png','female'=>'../img/avatar3.png');
						$userControllerObject=new com\kaic\controllers\UserController();
						$teamMembersList=$userControllerObject->getTeamMembersList();
						foreach($teamMembersList as $teamMember){
						?>
						<div class="row oddrow" onclick="viewTeamMember(<?=$teamMember['userId'];?>)">
							<div class="col-lg-2 col-md-2">
								<div class="default-user-img">
									<img src="<?=$imgSrc[$teamMember['gender']];?>" class="" />
								</div>
							</div>	
							<div class="col-lg-10 col-md-10">
								<h4><?=$teamMember['fullName'];?></h4>
								<p><span class="badge bg-yellow">Team Member</span> <span class="badge bg-green">Can Log in</span></p>
							</div>						
					</div>
						
						<?php	
						}
						
					?>
						
					
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->
		
		
		
		<!-- Create New Client MODAL -->
		<form id="teamMemberForm" action="../router.php" method="post">
        <div class="modal fade" id="add-team-member-modal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" style="width: 800px;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Team Member</h4>
                    </div>
                    <form action="#" method="post">
                        <div class="modal-body">
                            <div class="box-body">
                                <div class="row">
                                	<div class="col-md-6">
                                    	<div class="row">
                                        	<div class="col-lg-4 col-md-4">
                                                <div class="default-user-img">
                                                    <img src="../img/avatar5.png" class="" />
                                                </div>
                                            </div>	
                                            <div class="col-lg-8 col-md-8">
                                                <div class="form-group">
                                                    <input type="text" name="fullName" placeholder="First name and Last name" class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <input type="text" name="jobTitle" placeholder="Job Title" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <hr/>
                                        <!-- row ends -->
										<div class="clear"></div>
										<div class="form-group">
											<div  class="col-md-3 pull-left label-adjust"><label for="">Phone</label></div>
                                            <div  class="col-md-9"><input type="text" name="phone" class="form-control"></div>
                                        </div>
										<div class="clear"></div>
										<div class="form-group">
											<div class="col-md-3 pull-left label-adjust"><label for="">Email</label></div>
                                            <div  class="col-md-9"><input type="text" name="email" class="form-control"></div>
                                        </div>
                                        <div class="form-group">
											<div  class="col-md-3 pull-left label-adjust"><label for="">Gender</label></div>
                                            <div  class="col-md-9">
												<div class="form-group">                                    
													<label>
														<input type="radio" name="gender" value="female" vclass="minimal" checked/>
													</label>
													<label>
														&nbsp;&nbsp;Female
													</label>
													<label>
														&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" value="male" name="gender" class="minimal"/>                                                    
													</label>
													<label>
														&nbsp;&nbsp;Male
													</label>
												</div>
											</div>
                                        </div>
										<div class="clear"></div>
										<div class="form-group">
											<div class="col-md-3 pull-left label-adjust"><label for="exampleInputEmail1">About</label></div>
                                            <div class="col-md-9">
												<textarea name="about" class="form-control" rows="3"></textarea>
											</div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                    	<h5><strong>What services can be booked for this employee online?</strong></h5>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox"/>
                                                &nbsp;All services
                                            </label>                
                                        </div>
                                        <div id="ServicesList">
                                           
                                           <?php 
											$serviceCategories=new com\kaic\controllers\ServiceCategoriesController();
											$servicesObject=new com\kaic\controllers\ServicesController();
											$serviceCategoriesList=$serviceCategories->getServiceCategoriesList();
											foreach($serviceCategoriesList as $serviceCategory){
											?>
											<div class="serviceHeading">
                                                <span><strong><?=$serviceCategory['serviceCategoryName']?></strong></span>
                                            </div>	
											<?php

											$serviceList=$servicesObject->getServiceListByCategoryID($serviceCategory['serviceCategoryId'],'service');
											foreach($serviceList as $service){
											?>
											<div class="checkbox">
                                                <label>
                                                    <input name="services[]" value="<?=$service['serviceId']?>" type="checkbox"/>&nbsp;<?=$service['serviceName']?>
                                                </label>                
                                            </div>
											<?php 	
											}
											}
					
											?>
                                           
                                            
                                        </div>
                                        
                                    </div>
                                    
                                </div>
                            </div><!-- /.box-body -->
                        </div>
                        <div class="modal-footer clearfix">
                            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Cancel</button>
                            <button type="submit" class="btn btn-primary pull-left"><i class="fa fa-check"></i> Submit</button>
                            
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        </form>


       <?php
include_once('footer.inc');
?>


<script>
$("#teamMemberForm").on('submit',(function(e){
	e.preventDefault();
	$.ajax({
	url: '../router.php?controller=UserController&cmd=createTeamMember',
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(response, status, req) {
		if(response==1){
			alert("User Added Sucessfully");
		}
	  },
	error: function(){} 	        
	});
}));




function viewTeamMember(userId){
	//alert(userId);
	//$('#add-team-member-modal').modal('show');
}
</script>